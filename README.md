Maintainer: Alexandru Iulian Orhean (alexandru.orhean@gmail.com)

Python 3 program used for parsing and plotting experimental data generated from various benchmarks.
