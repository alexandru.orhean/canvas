import sys
from canvas import config
from canvas import tasks

import matplotlib
matplotlib.use('Agg')

MSG = "USAGE: python3 canvas.py <config file> <tasks file>"

def main(args):
    if len(args) != 3:
        print(MSG)
        sys.exit(1)
    
    cfg = config.Config(args[1])
    task_parser = tasks.TaskParser(args[2], cfg.getProjectRootDir())
    executor_parse_tasks = tasks.TaskExecutor(task_parser.getParseTasks(), cfg.getNumWorkerThreads())
    executor_plot_tasks = tasks.TaskExecutor(task_parser.getPlotTasks(), cfg.getNumWorkerThreads())

    print('INFO: *** starting parse tasks ***')
    executor_parse_tasks.execute()
    print('INFO: *** finished parse tasks and starting plot tasks ***')
    executor_plot_tasks.execute()
    print('INFO: *** finished plot tasks ***')

    sys.exit(0)

if __name__ == '__main__':
    main(sys.argv)