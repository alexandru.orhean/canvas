from collections import defaultdict
from statistics import mean

def parse_rd_tp(input_file: str, output_file: str):
    data = defaultdict(list)
    
    with open(input_file, 'r') as fstream:
        num_threads = None
        throughput = None
        for line in fstream:
            if line.find('Finished loading') != -1:
                words = line.split()
                num_threads = int(words[7])
            elif line.find('MB/sec') != -1:
                words = line.split()
                throughput = float(words[1])
                data[num_threads].append(throughput)
    
    outdata = list()
    mean_data = {key: mean(value) for (key, value) in data.items()}
    for (key, value) in mean_data.items():
        outdata.append("{} {}\n".format(key, value))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rd_numa_tp(input_file: str, output_file: str):
    data = defaultdict(list)
    
    with open(input_file, 'r') as fstream:
        num_threads = None
        throughput = None
        for line in fstream:
            if line.find('reader threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('MiB/sec') != -1:
                words = line.split()
                throughput = float(words[2])
                data[num_threads].append(throughput)
    
    outdata = list()
    mean_data = {key: mean(value) for (key, value) in data.items()}
    for (key, value) in mean_data.items():
        outdata.append("{} {}\n".format(key, value))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rd_numa_profile(input_file: str, output_file: str):
    data_reader_pop_time = defaultdict(list)
    data_reader_read_time = defaultdict(list)
    data_reader_push_time = defaultdict(list)
    with open(input_file, 'r') as fstream:
        num_threads = None
        reader_time = None
        for line in fstream:
            if line.find('number of reader threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('reader pop times') != -1:
                words = line.split()
                reader_time = 0.0
                num_times = 0
                for elem in words[5:]:
                    reader_time += float(elem)
                    num_times += 1
                reader_time = reader_time / num_times
                data_reader_pop_time[num_threads].append(reader_time)
            elif line.find('reader read times') != -1:
                words = line.split()
                reader_time = 0.0
                num_times = 0
                for elem in words[5:]:
                    reader_time += float(elem)
                    num_times += 1
                reader_time = reader_time / num_times
                data_reader_read_time[num_threads].append(reader_time)
            elif line.find('reader push times') != -1:
                words = line.split()
                reader_time = 0.0
                num_times = 0
                for elem in words[5:]:
                    reader_time += float(elem)
                    num_times += 1
                reader_time = reader_time / num_times
                data_reader_push_time[num_threads].append(reader_time)
    
    outdata = list()
    mean_data_reader_pop_time = {key: mean(value) for (key, value) in data_reader_pop_time.items()}
    mean_data_reader_read_time = {key: mean(value) for (key, value) in data_reader_read_time.items()}
    mean_data_reader_push_time = {key: mean(value) for (key, value) in data_reader_push_time.items()}
    for key in mean_data_reader_pop_time:
        outdata.append("{} {} {} {}\n".format(key,
                                              mean_data_reader_pop_time[key],
                                              mean_data_reader_read_time[key],
                                              mean_data_reader_push_time[key]))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rd_blk_tp(input_file: str, output_file: str):
    data = defaultdict(lambda: defaultdict(list))
    
    with open(input_file, 'r') as fstream:
        num_threads = None
        block_size = None
        throughput = None
        for line in fstream:
            if line.find('Finished loading') != -1:
                words = line.split()
                num_threads = int(words[7])
                block_size = int(words[14])
            elif line.find('MB/sec') != -1:
                words = line.split()
                throughput = float(words[1])
                data[num_threads][block_size].append(throughput)
    
    outdata = list()
    mean_data = {key1: {key2: mean(value2) for (key2, value2) in value1.items()} for (key1, value1) in data.items()}
    for (key1, value1) in mean_data.items():
        for (key2, value2) in value1.items():
            outdata.append("{} {} {}\n".format(key1, key2, value2))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rd_numa_blk_tp(input_file: str, output_file: str):
    data = defaultdict(lambda: defaultdict(list))
    
    with open(input_file, 'r') as fstream:
        num_threads = None
        block_size = None
        throughput = None
        for line in fstream:
            if line.find('reader threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('block size') != -1:
                words = line.split()
                block_size = int(words[3])
            elif line.find('MiB/sec') != -1:
                words = line.split()
                throughput = float(words[2])
                data[num_threads][block_size].append(throughput)
    
    outdata = list()
    mean_data = {key1: {key2: mean(value2) for (key2, value2) in value1.items()} for (key1, value1) in data.items()}
    for (key1, value1) in mean_data.items():
        for (key2, value2) in value1.items():
            outdata.append("{} {} {}\n".format(key1, key2, value2))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rd_iozone_blk_tp(input_file: str, output_file: str):
    data = defaultdict(lambda: defaultdict(list))
    
    with open(input_file, 'r') as fstream:
        num_threads = None
        block_size = None
        throughput = None
        for line in fstream:
            if line.find('Throughput test with') != -1:
                words = line.split()
                num_threads = int(words[3])
            elif line.find('Record Size') != -1:
                words = line.split()
                block_size = int(words[2]) * 1024
            elif line.find('Parent sees throughput') != -1 and line.find(' readers') != -1:
                words = line.split()
                throughput = float(words[7]) / 1024
                data[num_threads][block_size].append(throughput)
    
    outdata = list()
    mean_data = {key1: {key2: mean(value2) for (key2, value2) in value1.items()} for (key1, value1) in data.items()}
    for (key1, value1) in mean_data.items():
        for (key2, value2) in value1.items():
            outdata.append("{} {} {}\n".format(key1, key2, value2))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rd_ior_blk_tp(input_file: str, output_file: str):
    data = defaultdict(lambda: defaultdict(list))
    
    with open(input_file, 'r') as fstream:
        num_threads = None
        block_size = None
        throughput = None
        for line in fstream:
            if line.find('tasks    ') != -1:
                words = line.split()
                num_threads = int(words[2])
            elif line.find('Command line') != -1:
                words = line.split()
                block_size = int(words[10])
            elif line.find('Max Read') != -1:
                words = line.split()
                throughput = float(words[2])
                data[num_threads][block_size].append(throughput)
    
    outdata = list()
    mean_data = {key1: {key2: mean(value2) for (key2, value2) in value1.items()} for (key1, value1) in data.items()}
    for (key1, value1) in mean_data.items():
        for (key2, value2) in value1.items():
            outdata.append("{} {} {}\n".format(key1, key2, value2))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)
