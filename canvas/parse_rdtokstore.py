from collections import defaultdict
from statistics import mean

def parse_rdtokstore_tp(input_file: str, output_file: str):
    data = defaultdict(list)

    with open(input_file, 'r') as fstream:
        num_threads = None
        throughput = None
        for line in fstream:
            if line.find('number of tokenizer threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('MiB/sec') != -1:
                words = line.split()
                throughput = float(words[2])
                data[num_threads].append(throughput)
    
    outdata = list()
    mean_data = {key: mean(value) for (key, value) in data.items()}
    for (key, value) in mean_data.items():
        outdata.append("{} {}\n".format(key, value))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rdtokstore_tp_r(input_file: str, output_file: str):
    data = defaultdict(list)

    with open(input_file, 'r') as fstream:
        num_threads = None
        throughput = None
        for line in fstream:
            if line.find('number of reader threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('MiB/sec') != -1:
                words = line.split()
                throughput = float(words[2])
                data[num_threads].append(throughput)
    
    outdata = list()
    mean_data = {key: mean(value) for (key, value) in data.items()}
    for (key, value) in mean_data.items():
        outdata.append("{} {}\n".format(key, value))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rdtokstore_lat(input_file: str, output_file: str):
    data_init_time = defaultdict(list)
    data_exec_time = defaultdict(list)

    with open(input_file, 'r') as fstream:
        num_threads = None
        init_time = None
        exec_time = None
        for line in fstream:
            if line.find('number of tokenizer threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('total initialization time') != -1:
                words = line.split()
                init_time = float(words[4])
                data_init_time[num_threads].append(init_time)
            elif line.find('total execution time') != -1:
                words = line.split()
                exec_time = float(words[4])
                data_exec_time[num_threads].append(exec_time)
    
    outdata = list()
    mean_data_init_time = {key: mean(value) for (key, value) in data_init_time.items()}
    mean_data_exec_time = {key: mean(value) for (key, value) in data_exec_time.items()}
    for key in mean_data_init_time:
        outdata.append("{} {} {}\n".format(key, mean_data_init_time[key], mean_data_exec_time[key]))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rdtokstore_profile(input_file: str, output_file: str):
    data_reader_pop_time = defaultdict(list)
    data_reader_read_time = defaultdict(list)
    data_reader_push_time = defaultdict(list)
    data_tok_pop_time = defaultdict(list)
    data_tok_tok_time = defaultdict(list)
    data_tok_store_time = defaultdict(list)
    data_tok_push_time = defaultdict(list)

    with open(input_file, 'r') as fstream:
        num_threads = None
        reader_time = None
        tok_time = None
        for line in fstream:
            if line.find('number of tokenizer threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('reader pop times') != -1:
                words = line.split()
                reader_time = 0.0
                num_times = 0
                for elem in words[5:]:
                    reader_time += float(elem)
                    num_times += 1
                reader_time = reader_time / num_times
                data_reader_pop_time[num_threads].append(reader_time)
            elif line.find('reader read times') != -1:
                words = line.split()
                reader_time = 0.0
                num_times = 0
                for elem in words[5:]:
                    reader_time += float(elem)
                    num_times += 1
                reader_time = reader_time / num_times
                data_reader_read_time[num_threads].append(reader_time)
            elif line.find('reader push times') != -1:
                words = line.split()
                reader_time = 0.0
                num_times = 0
                for elem in words[5:]:
                    reader_time += float(elem)
                    num_times += 1
                reader_time = reader_time / num_times
                data_reader_push_time[num_threads].append(reader_time)
            elif line.find('tokenizer pop times') != -1:
                words = line.split()
                tok_time = 0.0
                num_times = 0
                for elem in words[5:]:
                    tok_time += float(elem)
                    num_times += 1
                tok_time = tok_time / num_times
                data_tok_pop_time[num_threads].append(tok_time)
            elif line.find('tokenizer tokenize times') != -1:
                words = line.split()
                tok_time = 0.0
                num_times = 0
                for elem in words[5:]:
                    tok_time += float(elem)
                    num_times += 1
                tok_time = tok_time / num_times
                data_tok_tok_time[num_threads].append(tok_time)
            elif line.find('tokenizer store times') != -1:
                words = line.split()
                tok_time = 0.0
                num_times = 0
                for elem in words[5:]:
                    tok_time += float(elem)
                    num_times += 1
                tok_time = tok_time / num_times
                data_tok_store_time[num_threads].append(tok_time)
            elif line.find('tokenizer push times') != -1:
                words = line.split()
                tok_time = 0.0
                num_times = 0
                for elem in words[5:]:
                    tok_time += float(elem)
                    num_times += 1
                tok_time = tok_time / num_times
                data_tok_push_time[num_threads].append(tok_time)
    
    outdata = list()
    mean_data_reader_pop_time = {key: mean(value) for (key, value) in data_reader_pop_time.items()}
    mean_data_reader_read_time = {key: mean(value) for (key, value) in data_reader_read_time.items()}
    mean_data_reader_push_time = {key: mean(value) for (key, value) in data_reader_push_time.items()}
    mean_data_tok_pop_time = {key: mean(value) for (key, value) in data_tok_pop_time.items()}
    mean_data_tok_tok_time = {key: mean(value) for (key, value) in data_tok_tok_time.items()}
    mean_data_tok_store_time = {key: mean(value) for (key, value) in data_tok_store_time.items()}
    mean_data_tok_push_time = {key: mean(value) for (key, value) in data_tok_push_time.items()}
    for key in mean_data_reader_pop_time:
        outdata.append("{} {} {} {} {} {} {} {}\n".format(key,
                                                          mean_data_reader_pop_time[key],
                                                          mean_data_reader_read_time[key],
                                                          mean_data_reader_push_time[key],
                                                          mean_data_tok_pop_time[key],
                                                          mean_data_tok_tok_time[key],
                                                          mean_data_tok_store_time[key],
                                                          mean_data_tok_push_time[key]))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)
