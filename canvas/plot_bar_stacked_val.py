from canvas import plot_database

import matplotlib.pyplot as plt

def plot_bar_stacked_val(input_file: str, labels: list, output_file: str, mode: str):
    num = len(labels)

    colors = plot_database.canvas_colors
    xticks = []
    xdata = []
    plot_labels = []
    table_labels = []
    xtable_labels = []

    data = []
    for i in range(0, num):
        d = {'xdata': [], 'ydata': []}
        with open(input_file, 'r') as filein:
            for line in filein:
                words = line.replace('\n', '').split()
                d['xdata'].append(int(words[0]))
                d['ydata'].append(float(words[i + 1]))
        data.append(d)
        table_labels.append('(#' + str(i + 1) + ')')
        plot_labels.append(labels[i] + '(#' + str(i + 1) + ')')

    step = (1 + 2) * 2
    for i in range(0, len(data[0]['xdata'])):
        xtable_labels.append(str(data[0]['xdata'][i]))
        part_xticks = [''] * (step // 2)
        part_xticks[step // 4] = str(data[0]['xdata'][i])
        xticks += part_xticks
        xdata += [(step * i) + (j * 2) + 1 for j in range(0, (step // 2))]

    fig, axs = plt.subplots(2, 1, gridspec_kw={'height_ratios': [9, 1]})
    fig.tight_layout(h_pad=-1)

    tdata = []
    ydata_prev = None
    for i in range(0, len(data)):
        zdata = []
        ydata = []
        for j in range(0, len(data[i]['xdata'])):
            zdata.append((step * j) + (0 * 2) + 3)
            ydata.append(data[i]['ydata'][j])
        if ydata_prev is None:
            axs[0].bar(zdata, ydata, 2.0, zorder=2, edgecolor='black', color=colors[i], alpha=1.0, label=plot_labels[i])
            ydata_prev = ydata
        else:
            axs[0].bar(zdata, ydata, 2.0, zorder=2, bottom=ydata_prev, edgecolor='black', color=colors[i], alpha=1.0, label=plot_labels[i])
            ydata_prev = [elem1 + elem2 for (elem1, elem2) in zip(ydata_prev, ydata)]
        tdata.append(['%1.2f' % x for x in ydata])
    
    axs[1].table(cellText=tdata, rowLabels=table_labels, colLabels=xtable_labels, loc='bottom')
    axs[1].axis('off')

    axs[0].set_xlabel(plot_database.canvas_xlabel[mode])
    axs[0].legend(loc=plot_database.canvas_legend[mode])
    axs[0].set(xticks=xdata, xticklabels=xticks)
    axs[0].set_ylabel(plot_database.canvas_ylabel[mode])
    
    axs[0].grid(axis='y', b=True, zorder=1)
    plt.savefig(output_file, dpi=300, bbox_inches='tight')