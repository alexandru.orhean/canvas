from canvas import plot_database

import matplotlib.pyplot as plt

def plot_bar(input_files: list, labels: list, output_file: str, mode: str):
    num = len(labels)

    colors = plot_database.canvas_colors
    xticks = []
    xdata = []
    plot_labels = []
    table_labels = []
    xtable_labels = []

    font_size=14.0

    data = []
    for i in range(0, num):
        d = {'xdata': [], 'ydata': []}
        with open(input_files[i], 'r') as filein:
            for line in filein:
                words = line.replace('\n', '').split()
                d['xdata'].append(int(words[0]))
                d['ydata'].append(float(words[1]))
        data.append(d)
        table_labels.append('(#' + str(i + 1) + ')')
        plot_labels.append(labels[i] + '(#' + str(i + 1) + ')')

    step = (num + 2) * 2
    for i in range(0, len(data[0]['xdata'])):
        xtable_labels.append(str(data[0]['xdata'][i]))
        part_xticks = [''] * (step // 2)
        part_xticks[step // 4] = str(data[0]['xdata'][i])
        xticks += part_xticks
        xdata += [(step * i) + (j * 2) + 1 for j in range(0, (step // 2))]

    ax = plt.subplot()

    tdata = []
    for i in range(0, len(data)):
        zdata = []
        ydata = []
        for j in range(0, len(data[i]['xdata'])):
            zdata.append((step * j) + (i * 2) + 4)
            ydata.append(data[i]['ydata'][j])
        ax.bar(zdata, ydata, 2.0, zorder=2, edgecolor='black', color=colors[i], alpha=1.0, label=plot_labels[i])
        tdata.append(['%1.2f' % x for x in ydata])

    ax.set_xlabel(plot_database.canvas_xlabel[mode], fontsize=font_size)
    ax.set_ylabel(plot_database.canvas_ylabel[mode], fontsize=font_size)
    ax.legend(loc=plot_database.canvas_legend[mode], fontsize=font_size)
    ax.set(xticks=xdata, xticklabels=xticks)
    ax.tick_params(axis='both', labelsize=font_size)

    if mode in plot_database.canvas_ylimit:
        ax.set_ylim([0.0, plot_database.canvas_ylimit[mode]])
    
    ax.grid(axis='y', b=True, zorder=1)
    plt.savefig(output_file, dpi=300, bbox_inches='tight')
