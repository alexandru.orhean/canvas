
def transform_in_bytes(x: int) -> str:
    if x < 1024:
        return '{}B'.format(x)
    elif x < (1024 * 1024):
        return '{}KiB'.format(x // 1024)
    elif x < (1024 * 1024 * 1024):
        return '{}MiB'.format(x // (1024 * 1024))
    else:
        return '{}GiB'.format(x // (1024 * 1024 * 1024))
