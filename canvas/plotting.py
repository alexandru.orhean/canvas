from canvas.plot_bar import plot_bar
from canvas.plot_bar_val import plot_bar_val
from canvas.plot_bar_stacked_val import plot_bar_stacked_val
from canvas.plot_bar_val_nested import plot_bar_val_nested
from canvas import plot_database
from canvas.transformations import transform_in_bytes

class PlotException(Exception):
    def __init__(self, message):
        self.message = message
        super().__init__(message)
    
    def __str__(self) -> str:
        return self.message


def main_plot(args: dict, taskID: str, projectRootDir: str):
    if 'PLOT_MODE' not in args:
        print('ERROR: PLOT_MODE is unset in {}'.format(taskID))
        raise PlotException('PLOT_MODE is unset')
    if 'PLOT_TYPE' not in args:
        print('ERROR: PLOT_TYPE is unset in {}'.format(taskID))
        raise PlotException('PLOT_TYPE is unset')

    plot_mode = args['PLOT_MODE'][0]
    if plot_mode not in plot_database.canvas_xlabel:
        print('ERROR: unrecognized PLOT_MODE "{}" in "{}"'.format(plot_mode, taskID))
        raise PlotException('unrecognized PLOT_MODE')
    
    plot_type = args['PLOT_TYPE'][0]
    if plot_type == 'bar-val':
        if 'INPUT_FILES' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise PlotException('INPUT_FILE is unset')
        if 'PLOT_LABELS' not in args:
            print('ERROR: PLOT_LABELS is unset in {}'.format(taskID))
            raise PlotException('PLOT_LABELS is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise PlotException('OUTPUT_MODE is unset')
        
        input_files = [(projectRootDir + '/' + elem).replace('//', '/') for elem in args['INPUT_FILES']]
        plot_labels = args['PLOT_LABELS']
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        plot_bar_val(input_files, plot_labels, output_file, plot_mode)
    elif plot_type == 'bar':
        if 'INPUT_FILES' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise PlotException('INPUT_FILE is unset')
        if 'PLOT_LABELS' not in args:
            print('ERROR: PLOT_LABELS is unset in {}'.format(taskID))
            raise PlotException('PLOT_LABELS is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise PlotException('OUTPUT_MODE is unset')
        
        input_files = [(projectRootDir + '/' + elem).replace('//', '/') for elem in args['INPUT_FILES']]
        plot_labels = args['PLOT_LABELS']
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        plot_bar(input_files, plot_labels, output_file, plot_mode)
    elif plot_type == 'bar-val-stacked':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise PlotException('INPUT_FILE is unset')
        if 'PLOT_LABELS' not in args:
            print('ERROR: PLOT_LABELS is unset in {}'.format(taskID))
            raise PlotException('PLOT_LABELS is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise PlotException('OUTPUT_MODE is unset')
        
        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        plot_labels = args['PLOT_LABELS']
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        plot_bar_stacked_val(input_file, plot_labels, output_file, plot_mode)
    elif plot_type == 'bar-val-nested-blk':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise PlotException('INPUT_FILE is unset')
        if 'PLOT_LABEL' not in args:
            print('ERROR: PLOT_LABEL is unset in {}'.format(taskID))
            raise PlotException('PLOT_LABEL is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise PlotException('OUTPUT_MODE is unset')

        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        plot_label = args['PLOT_LABEL'][0]
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        plot_bar_val_nested(input_file, plot_label, output_file, plot_mode, transform_in_bytes)
    else:
        print('ERROR: unrecognized PLOT_TYPE "{}" in "{}"'.format(plot_type, taskID))
        raise PlotException('unrecognized PLOT_TYPE')
