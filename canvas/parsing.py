from canvas.parse_rd import parse_rd_tp
from canvas.parse_rd import parse_rd_numa_tp
from canvas.parse_rd import parse_rd_numa_profile
from canvas.parse_rd import parse_rd_blk_tp
from canvas.parse_rd import parse_rd_numa_blk_tp
from canvas.parse_rd import parse_rd_iozone_blk_tp
from canvas.parse_rd import parse_rd_ior_blk_tp
from canvas.parse_rdtok import parse_rdtok_tp
from canvas.parse_rdtok import parse_rdtok_numa_tp
from canvas.parse_rdtokstore import parse_rdtokstore_tp
from canvas.parse_rdtokstore import parse_rdtokstore_tp_r
from canvas.parse_rdtokstore import parse_rdtokstore_lat
from canvas.parse_rdtokstore import parse_rdtokstore_profile
from canvas.parse_rdtokidx import parse_rdtokidx_qlat
from canvas.parse_rdtokidx import parse_rdtokidx_tp
from canvas.parse_rdtokidx import parse_rdtokidx_tp_plus

class ParseException(Exception):
    def __init__(self, message):
        self.message = message
        super().__init__(message)
    
    def __str__(self) -> str:
        return self.message


def main_parse(args: dict, taskID: str, projectRootDir: str):
    if 'PARSE_MODE' not in args:
        print('ERROR: PARSE_MODE is unset in {}'.format(taskID))
        raise ParseException('PARSE_MODE is unset')
        
    parse_mode = args['PARSE_MODE'][0]
    if parse_mode == 'rd-tp':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')
        
        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rd_tp(input_file, output_file)
    elif parse_mode == 'rd-numa-tp':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')
        
        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rd_numa_tp(input_file, output_file)
    elif parse_mode == 'rd-numa-profile':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')

        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rd_numa_profile(input_file, output_file)
    elif parse_mode == 'rdtok-tp':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')
        
        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rdtok_tp(input_file, output_file)
    elif parse_mode == 'rdtok-numa-tp':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')
        
        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rdtok_numa_tp(input_file, output_file)
    elif parse_mode == 'rdtokstore-tp':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')

        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rdtokstore_tp(input_file, output_file)
    elif parse_mode == 'rdtokstore-tp-r':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')

        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rdtokstore_tp_r(input_file, output_file)
    elif parse_mode == 'rdtokstore-lat':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')

        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rdtokstore_lat(input_file, output_file)
    elif parse_mode == 'rdtokstore-profile':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')

        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rdtokstore_profile(input_file, output_file)
    elif parse_mode == 'rd-blk-tp':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')
        
        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rd_blk_tp(input_file, output_file)
    elif parse_mode == 'rd-numa-blk-tp':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')
        
        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rd_numa_blk_tp(input_file, output_file)
    elif parse_mode == 'rd-iozone-blk-tp':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')
        
        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rd_iozone_blk_tp(input_file, output_file)
    elif parse_mode == 'rd-ior-blk-tp':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')
        
        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rd_ior_blk_tp(input_file, output_file)
    elif parse_mode == 'rdtokidx-qlat':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')

        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rdtokidx_qlat(input_file, output_file)
    elif parse_mode == 'rdtokidx-tp':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')

        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rdtokidx_tp(input_file, output_file)
    elif parse_mode == 'rdtokidx-tp-plus':
        if 'INPUT_FILE' not in args:
            print('ERROR: INPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('INPUT_FILE is unset')
        if 'OUTPUT_FILE' not in args:
            print('ERROR: OUTPUT_FILE is unset in {}'.format(taskID))
            raise ParseException('OUTPUT_MODE is unset')

        input_file = (projectRootDir + '/' + args['INPUT_FILE'][0]).replace('//', '/')
        output_file = (projectRootDir + '/' + args['OUTPUT_FILE'][0]).replace('//', '/')
        parse_rdtokidx_tp_plus(input_file, output_file)
    else:
        print('ERROR: unrecognized PARSE_MODE "{}" in "{}"'.format(parse_mode, taskID))
        raise ParseException('unrecognized PARSE_MODE')
