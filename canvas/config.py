
class ConfigException(Exception):
    def __init__(self, message):
        self.message = message
        super().__init__(message)
    
    def __str__(self) -> str:
        return self.message


class Config:
    def __init__(self, configFile: str):
        self.projectRootDir = None
        self.numWorkers = 1
        with open(configFile, 'r') as fStream:
            for line in fStream:
                entry = line.replace('\n', '').replace(' ', '').split(':')
                if entry[0] == 'PROJECT_ROOT_DIR':
                    self.projectRootDir = entry[1]
                elif entry[0] == 'NUM_WORKERS':
                    self.numWorkers = int(entry[1])
                else:
                    print('WARN: unrecognized configuration parameter "{}"'.format(entry[0]))
        
        if self.projectRootDir is None:
            print('FATAL: the project root directory was not configured')
            raise ConfigException('project root directory not defined')
    
    def getProjectRootDir(self) -> str:
        return self.projectRootDir
    
    def getNumWorkerThreads(self) -> int:
        return self.numWorkers
