from canvas import parsing
from canvas import plotting
import multiprocessing as mp
from collections import defaultdict
import traceback

class TaskException(Exception):
    def __init__(self, message):
        self.message = message
        super().__init__(message)
    
    def __str__(self) -> str:
        return self.message


class Task:
    def __init__(self, taskFile: str, projectRootDir: str):
        self.taskFile = (projectRootDir + '/' + taskFile).replace('//', '/')
        self.taskID = None
        self.taskType = None
        self.taskArgs = defaultdict(list)
        self.projectRootDir = projectRootDir
        with open(self.taskFile, 'r') as fStream:
            for line in fStream:
                entry = line.replace('\n', '').replace(' ', '').split(':')
                if entry[0] == 'TASK_ID':
                    self.taskID = entry[1]
                elif entry[0] == 'TASK_TYPE':
                    self.taskType = entry[1]
                else:
                    self.taskArgs[entry[0]].append(entry[1])
        
        if self.taskID is None:
            print('ERROR: ID not set for task defined in "{}"'.format(self.taskFile))
            raise TaskException('task ID not defined not defined')
        if self.taskType is None:
            print('ERROR: type not set for task defined in "{}"'.format(self.taskFile))
            raise TaskException('task type not defined not defined')
    
    def getType(self) -> str:
        return self.taskType
    
    def getFile(self) -> str:
        return self.taskFile
    
    def execute(self):
        if self.taskType == 'parse':
            try:
                parsing.main_parse(self.taskArgs, self.taskID, self.projectRootDir)
                print('INFO: finished executing task "{}"'.format(self.taskID))
            except parsing.ParseException as e:
                print(e)
            except:
                traceback.print_exc()
        elif self.taskType == 'plot':
            try:
                plotting.main_plot(self.taskArgs, self.taskID, self.projectRootDir)
                print('INFO: finished executing task "{}"'.format(self.taskID))
            except plotting.PlotException as e:
                print(e)
            except:
                traceback.print_exc()
        else:
            print('WARN: did nothing to "{}"'.format(self.taskFile))


class TaskParser:
    def __init__(self, tasksFile: str, projectRootDir: str):
        self.parseTasks = list()
        self.plotTasks = list()
        with open(tasksFile, 'r') as fStream:
            for line in fStream:
                # added support for commenting tasks
                if line[0] == '#':
                    continue
                taskFile = line.replace('\n', '')
                try:
                    task = Task(taskFile, projectRootDir)
                    if task.getType() == 'parse':
                        self.parseTasks.append(task)
                    elif task.getType() == 'plot':
                        self.plotTasks.append(task)
                    else:
                        print('ERROR: unrecognized task type "{}"'.format(task.getFile()))
                except TaskException as e:
                    print(e)
    
    def getParseTasks(self) -> list:
        return self.parseTasks
    
    def getPlotTasks(self) -> list:
        return self.plotTasks
                

class TaskExecutor:
    def __init__(self, tasks: list, numWorkers: int):
        self.tasks = tasks
        self.numWorkers = numWorkers
    
    def executeTask(self, task: Task):
        task.execute()
    
    def execute(self):
        with mp.Pool(processes=self.numWorkers) as p:
            p.map(self.executeTask, self.tasks)
