from canvas import plot_database

import matplotlib.pyplot as plt
from collections import defaultdict

def plot_bar_val_nested(input_file: str, label: str, output_file: str, mode: str, xtransform):
    num = 0

    colors = plot_database.canvas_colors
    xticks = []
    xdata = []
    plot_labels = []
    table_labels = []
    xtable_labels = []

    d = defaultdict(lambda: defaultdict(list))
    with open(input_file, 'r') as filein:
        for line in filein:
            words = line.replace('\n', '').split()
            d[int(words[0])]['xdata'].append(int(words[1]))
            d[int(words[0])]['ydata'].append(float(words[2]))
    
    data = []
    for key in d:
        data.append(d[key])
        table_labels.append('(# {})'.format(num + 1))
        plot_labels.append('{}-{} (# {})'.format(key, label, num + 1))
        num += 1

    step = (num + 2) * 2
    for i in range(0, len(data[0]['xdata'])):
        xtable_labels.append(xtransform(data[0]['xdata'][i]))
        part_xticks = [''] * (step // 2)
        part_xticks[step // 4] = xtransform(data[0]['xdata'][i])
        xticks += part_xticks
        xdata += [(step * i) + (j * 2) + 1 for j in range(0, (step // 2))]

    fig, axs = plt.subplots(2, 1, gridspec_kw={'height_ratios': [9, 1]})
    fig.tight_layout(h_pad=-1)

    max_ydata = 0.0
    tdata = []
    for i in range(0, len(data)):
        zdata = []
        ydata = []
        for j in range(0, len(data[i]['xdata'])):
            zdata.append((step * j) + (i * 2) + 4)
            ydata.append(data[i]['ydata'][j])
        axs[0].bar(zdata, ydata, 2.0, zorder=2, edgecolor='black', color=colors[i], alpha=1.0, label=plot_labels[i])
        tdata.append(['%1.2f' % x for x in ydata])
        local_max_ydata = max(ydata)
        if local_max_ydata > max_ydata:
            max_ydata = local_max_ydata
    
    axs[1].table(cellText=tdata, rowLabels=table_labels, colLabels=xtable_labels, loc='bottom')
    axs[1].axis('off')

    axs[0].set_xlabel(plot_database.canvas_xlabel[mode])
    axs[0].legend(loc=plot_database.canvas_legend[mode])
    axs[0].set(xticks=xdata, xticklabels=xticks)
    axs[0].set_ylabel(plot_database.canvas_ylabel[mode])
    axs[0].set_ylim([0, max_ydata * 2])
    
    axs[0].grid(axis='y', b=True, zorder=1)
    plt.savefig(output_file, dpi=300, bbox_inches='tight')
