from collections import defaultdict
from statistics import mean

def parse_rdtokidx_qlat(input_file: str, output_file: str):
    data = defaultdict(list)

    with open(input_file, 'r') as fstream:
        num_threads = None
        throughput = None
        for line in fstream:
            if line.find('number of indexing threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('total search time') != -1:
                words = line.split()
                throughput = float(words[4])
                data[num_threads].append(throughput)
    
    outdata = list()
    mean_data = {key: mean(value) for (key, value) in data.items()}
    for (key, value) in mean_data.items():
        outdata.append("{} {}\n".format(key, value))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rdtokidx_tp(input_file: str, output_file: str):
    data = defaultdict(list)

    with open(input_file, 'r') as fstream:
        num_threads = None
        throughput = None
        for line in fstream:
            if line.find('number of indexing threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('MiB/sec') != -1:
                words = line.split()
                throughput = float(words[2])
                data[num_threads].append(throughput)
    
    outdata = list()
    mean_data = {key: mean(value) for (key, value) in data.items()}
    for (key, value) in mean_data.items():
        outdata.append("{} {}\n".format(key, value))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rdtokidx_tp_plus(input_file: str, output_file: str):
    data = defaultdict(list)

    with open(input_file, 'r') as fstream:
        num_threads = None
        throughput = None
        for line in fstream:
            if line.find('number of reader threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('number of indexing threads') != -1:
                words = line.split()
                num_threads += int(words[5])
            elif line.find('MiB/sec') != -1:
                words = line.split()
                throughput = float(words[2])
                data[num_threads].append(throughput)
    
    outdata = list()
    mean_data = {key: mean(value) for (key, value) in data.items()}
    for (key, value) in mean_data.items():
        outdata.append("{} {}\n".format(key, value))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)