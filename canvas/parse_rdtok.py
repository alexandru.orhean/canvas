from collections import defaultdict
from statistics import mean

def parse_rdtok_tp(input_file: str, output_file: str):
    data = defaultdict(list)

    with open(input_file, 'r') as fstream:
        num_threads = None
        throughput = None
        for line in fstream:
            if line.find('Finished tokenizing') != -1:
                words = line.split()
                num_threads = int(words[7]) + int(words[9])
            elif line.find('MB/sec') != -1:
                words = line.split()
                throughput = float(words[1])
                data[num_threads].append(throughput)
    
    outdata = list()
    mean_data = {key: mean(value) for (key, value) in data.items()}
    for (key, value) in mean_data.items():
        outdata.append("{} {}\n".format(key, value))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)


def parse_rdtok_numa_tp(input_file: str, output_file: str):
    data = defaultdict(list)

    with open(input_file, 'r') as fstream:
        num_threads = None
        throughput = None
        for line in fstream:
            if line.find('number of reader threads') != -1:
                words = line.split()
                num_threads = int(words[5])
            elif line.find('number of tokenizer threads') != -1:
                words = line.split()
                num_threads += int(words[5])
            elif line.find('MiB/sec') != -1:
                words = line.split()
                throughput = float(words[2])
                data[num_threads].append(throughput)
    
    outdata = list()
    mean_data = {key: mean(value) for (key, value) in data.items()}
    for (key, value) in mean_data.items():
        outdata.append("{} {}\n".format(key, value))
    
    with open(output_file, 'w+') as fstream:
        fstream.writelines(outdata)

