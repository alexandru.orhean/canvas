from matplotlib.cm import get_cmap

matplt_colors = [x for x in get_cmap('tab20').colors]
canvas_colors = [matplt_colors[0], matplt_colors[2], matplt_colors[4], matplt_colors[6], matplt_colors[8],
                 matplt_colors[16], matplt_colors[18], matplt_colors[14],
                 matplt_colors[1], matplt_colors[3], matplt_colors[5], matplt_colors[7], matplt_colors[9],
                 matplt_colors[17], matplt_colors[19], matplt_colors[15]]

canvas_xlabel = {
    'rd-tp': 'Number of threads',
    'rd-tp-paper': 'Number of read threads',
    'rd-profile': 'Number of threads',
    'rdtokstore-tp-8': 'Number of tokenizer threads (plus 8 reader threads)',
    'rdtokstore-tp-16': 'Number of tokenizer threads (plus 16 reader threads)',
    'rdtokstore-tp-48': 'Number of tokenizer threads (plus 48 reader threads)',
    'rdtokstore-tp-r-32': 'Number of reader threads (plus 32 tokenzier threads)',
    'rdtokstore-tp-r-64': 'Number of reader threads (plus 64 tokenzier threads)',
    'rdtokstore-lat-16': 'Number of threads (plus 16 reader threads)',
    'rdtokstore-profile-16': 'Number of threads (plus 16 reader threads)',
    'rd-blk-tp': 'Block size',
    'rd-blk-tp-r': 'Block size',
    'idx-tp': 'N number of indexer threads',
    'rdtokidx-tp': 'N number of indexer threads',
    'rdtokidx-tp-paper': 'N index threads (plus another N read threads)',
    'rdtokidx-tp-8': 'N number of indexer threads (plus min(N, 8) reader threads)',
    'rdtokidx-tp-8-paper': 'N number of indexer threads (plus min(N, 8) reader threads)',
    'rdtokidx-tp-16': 'N number of indexer threads (plus min(N, 16) reader threads)',
    'rdtokidx-tp-16-paper': 'N number of indexer threads (plus min(N, 16) reader threads)',
    'rdtokidx-tp-48': 'N number of indexer threads (plus min(N, 48) reader threads)',
    'rdtokidx-tp-96': 'N number of indexer threads (plus min(N, 96) reader threads)',
    'rdtokidx-tp-96-paper': 'N number of indexer threads (plus min(N, 96) reader threads)',
    'rdtok-tp': 'Number of reader plus tokenizer threads',
    'rdtok-tp-paper': 'Number of read plus tokenize threads'
}

canvas_ylabel = {
    'rd-tp': 'Throughput (MiB/s)',
    'rd-tp-paper': 'Throughput (MiB/s)',
    'rd-profile': 'Microseconds',
    'rdtokstore-tp-8': 'Throughput (MiB/s)',
    'rdtokstore-tp-16': 'Throughput (MiB/s)',
    'rdtokstore-tp-48': 'Throughput (MiB/s)',
    'rdtokstore-tp-r-32': 'Throughput (MiB/s)',
    'rdtokstore-tp-r-64': 'Throughput (MiB/s)',
    'rdtokstore-lat-16': 'Seconds',
    'rdtokstore-profile-16': 'Microseconds',
    'rd-blk-tp': 'Throughput (MiB/s)',
    'rd-blk-tp-r': 'Throughput (MiB/s)',
    'idx-tp': 'Throughput (MiB/s)',
    'rdtokidx-tp': 'Throughput (MiB/s)',
    'rdtokidx-tp-paper': 'Throughput (MiB/s)',
    'rdtokidx-tp-8': 'Throughput (MiB/s)',
    'rdtokidx-tp-8-paper': 'Throughput (MiB/s)',
    'rdtokidx-tp-16': 'Throughput (MiB/s)',
    'rdtokidx-tp-16-paper': 'Throughput (MiB/s)',
    'rdtokidx-tp-48': 'Throughput (MiB/s)',
    'rdtokidx-tp-96': 'Throughput (MiB/s)',
    'rdtokidx-tp-96-paper': 'Throughput (MiB/s)',
    'rdtok-tp': 'Throughput (MiB/s)',
    'rdtok-tp-paper': 'Throughput (MiB/s)'
}

canvas_legend = {
    'rd-tp': 'upper left',
    'rd-tp-paper': 'upper left',
    'rd-profile': 'upper left',
    'rdtokstore-tp-8': 'upper left',
    'rdtokstore-tp-16': 'upper left',
    'rdtokstore-tp-48': 'upper left',
    'rdtokstore-tp-r-32': 'upper left',
    'rdtokstore-tp-r-64': 'upper left',
    'rdtokstore-lat-16': 'upper right',
    'rdtokstore-profile-16': 'upper right',
    'rd-blk-tp': 'upper left',
    'rd-blk-tp-r': 'upper right',
    'idx-tp': 'upper left',
    'rdtokidx-tp': 'upper left',
    'rdtokidx-tp-paper': 'upper left',
    'rdtokidx-tp-8': 'upper left',
    'rdtokidx-tp-8-paper': 'upper left',
    'rdtokidx-tp-16': 'upper left',
    'rdtokidx-tp-16-paper': 'upper left',
    'rdtokidx-tp-48': 'upper left',
    'rdtokidx-tp-96': 'upper left',
    'rdtokidx-tp-96-paper': 'upper left',
    'rdtok-tp': 'upper left',
    'rdtok-tp-paper': 'upper left'
}

canvas_ylimit = {
    'rd-tp-paper': 56000.00,
    'rdtok-tp-paper': 36000.00,
    'rdtokidx-tp-paper': 10200.00,
    'rdtokidx-tp-8-paper': 2900.00,
    'rdtokidx-tp-16-paper': 2900.00,
    'rdtokidx-tp-96-paper': 2900.00
}
